function initKonamiCode(_fct) {
	let code = [];
	let konamiCode = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
	let timeout;
	document.body.addEventListener("keydown", function (e) {
		clearTimeout(timeout);
		e = e || window.event;
		if (e.keyCode === konamiCode[code.length])
			code.push(e.keyCode);
		else
			code = [];
		if (code.length === konamiCode.length) _fct();
		if (code.length <= 1) {
			timeout = setTimeout(function () {
				code = [];
			}, 2000)
		}
	});
}

Console.log("LibKonamiCode from sorlinv is loaded");